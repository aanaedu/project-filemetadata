'use strict';
const uuidv4 = require('uuid/v4')
const path = require('path');
const fs = require('fs');

var express = require('express');
var cors = require('cors');
var multer  = require('multer');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const dir = './uploads';
    if (!fs.existsSync(dir)) {
      fs.mkdir(dir, err => cb(err, dir));
    }
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    const filename = uuidv4().replace(/-/g, '');
    const fileExtension = path.extname(file.originalname);
    cb(null, `${filename}${fileExtension}`);
  }
})
var upload = multer({ storage: storage }).single('upfile');


var app = express();

app.use(cors());
app.use('/public', express.static(process.cwd() + '/public'));

app.get('/', function (req, res) {
     res.sendFile(process.cwd() + '/views/index.html');
  });

app.get('/hello', function(req, res){
  res.json({greetings: "Hello, API"});
});


app.post('/api/fileanalyse', (req, res) => {
  upload(req, res, function (err) {
    if (err) {
      console.log('UploadFileError: ', err);
      return res.json({
        success: false,
        message: 'An error occured while uploading file. Please try again.'
      });
    }
    const { originalname, mimetype, size } = req.file;
    res.json({
      "name": originalname,
      "type": mimetype,
      "size": size
    });
  })
});


app.listen(process.env.PORT || 3000, function () {
  console.log('Node.js listening ...');
});
